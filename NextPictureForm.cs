﻿using System.Windows.Forms;

namespace Kufr
{
    public partial class NextPictureForm : Form
    {
        public NextPictureForm(string reseni, string souteziciBodyText)
        {
            InitializeComponent();
            ReseniLabel.Text = reseni + "\n\n" + souteziciBodyText;
            this.Height = 80 + ReseniLabel.Height + panel1.Height;
        }
    }
}
