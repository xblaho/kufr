﻿namespace Kufr
{
    partial class NextPictureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NextPictureButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ReseniLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // NextPictureButton
            // 
            this.NextPictureButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.NextPictureButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NextPictureButton.Location = new System.Drawing.Point(111, 5);
            this.NextPictureButton.Name = "NextPictureButton";
            this.NextPictureButton.Size = new System.Drawing.Size(231, 92);
            this.NextPictureButton.TabIndex = 0;
            this.NextPictureButton.Text = "Další obrázek";
            this.NextPictureButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.NextPictureButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 107);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(439, 100);
            this.panel1.TabIndex = 1;
            // 
            // ReseniLabel
            // 
            this.ReseniLabel.AutoSize = true;
            this.ReseniLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ReseniLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ReseniLabel.Location = new System.Drawing.Point(0, 0);
            this.ReseniLabel.Name = "ReseniLabel";
            this.ReseniLabel.Padding = new System.Windows.Forms.Padding(20, 10, 10, 10);
            this.ReseniLabel.Size = new System.Drawing.Size(206, 113);
            this.ReseniLabel.TabIndex = 1;
            this.ReseniLabel.Text = "ReseniLabel\r\n\r\nssdfsdf";
            this.ReseniLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NextPictureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 207);
            this.ControlBox = false;
            this.Controls.Add(this.ReseniLabel);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "NextPictureForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Další obrázek";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button NextPictureButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label ReseniLabel;
    }
}