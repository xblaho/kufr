﻿namespace Kufr
{
    partial class PictureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlp = new System.Windows.Forms.TableLayoutPanel();
            this.CellButtonA1 = new System.Windows.Forms.Button();
            this.CellButtonA3 = new System.Windows.Forms.Button();
            this.CellButtonA4 = new System.Windows.Forms.Button();
            this.CellButtonB1 = new System.Windows.Forms.Button();
            this.CellButtonB2 = new System.Windows.Forms.Button();
            this.CellButtonB3 = new System.Windows.Forms.Button();
            this.CellButtonB4 = new System.Windows.Forms.Button();
            this.CellButtonC1 = new System.Windows.Forms.Button();
            this.CellButtonC2 = new System.Windows.Forms.Button();
            this.CellButtonC3 = new System.Windows.Forms.Button();
            this.CellButtonC4 = new System.Windows.Forms.Button();
            this.CellButtonD1 = new System.Windows.Forms.Button();
            this.CellButtonD2 = new System.Windows.Forms.Button();
            this.CellButtonD3 = new System.Windows.Forms.Button();
            this.CellButtonD4 = new System.Windows.Forms.Button();
            this.CellButtonA2 = new System.Windows.Forms.Button();
            this.tlp.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlp
            // 
            this.tlp.BackColor = System.Drawing.Color.Transparent;
            this.tlp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tlp.ColumnCount = 4;
            this.tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp.Controls.Add(this.CellButtonA1, 0, 0);
            this.tlp.Controls.Add(this.CellButtonA3, 2, 0);
            this.tlp.Controls.Add(this.CellButtonA4, 3, 0);
            this.tlp.Controls.Add(this.CellButtonB1, 0, 1);
            this.tlp.Controls.Add(this.CellButtonB2, 1, 1);
            this.tlp.Controls.Add(this.CellButtonB3, 2, 1);
            this.tlp.Controls.Add(this.CellButtonB4, 3, 1);
            this.tlp.Controls.Add(this.CellButtonC1, 0, 2);
            this.tlp.Controls.Add(this.CellButtonC2, 1, 2);
            this.tlp.Controls.Add(this.CellButtonC3, 2, 2);
            this.tlp.Controls.Add(this.CellButtonC4, 3, 2);
            this.tlp.Controls.Add(this.CellButtonD1, 0, 3);
            this.tlp.Controls.Add(this.CellButtonD2, 1, 3);
            this.tlp.Controls.Add(this.CellButtonD3, 2, 3);
            this.tlp.Controls.Add(this.CellButtonD4, 3, 3);
            this.tlp.Controls.Add(this.CellButtonA2, 1, 0);
            this.tlp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp.Location = new System.Drawing.Point(0, 0);
            this.tlp.Margin = new System.Windows.Forms.Padding(0);
            this.tlp.Name = "tlp";
            this.tlp.RowCount = 4;
            this.tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp.Size = new System.Drawing.Size(800, 600);
            this.tlp.TabIndex = 2;
            // 
            // CellButtonA1
            // 
            this.CellButtonA1.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonA1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonA1.FlatAppearance.BorderSize = 0;
            this.CellButtonA1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonA1.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonA1.Location = new System.Drawing.Point(0, 0);
            this.CellButtonA1.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonA1.Name = "CellButtonA1";
            this.CellButtonA1.Size = new System.Drawing.Size(200, 150);
            this.CellButtonA1.TabIndex = 0;
            this.CellButtonA1.Text = "A1";
            this.CellButtonA1.UseVisualStyleBackColor = false;
            this.CellButtonA1.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonA3
            // 
            this.CellButtonA3.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonA3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonA3.FlatAppearance.BorderSize = 0;
            this.CellButtonA3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonA3.Location = new System.Drawing.Point(400, 0);
            this.CellButtonA3.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonA3.Name = "CellButtonA3";
            this.CellButtonA3.Size = new System.Drawing.Size(200, 150);
            this.CellButtonA3.TabIndex = 3;
            this.CellButtonA3.Text = "A3";
            this.CellButtonA3.UseVisualStyleBackColor = false;
            this.CellButtonA3.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonA4
            // 
            this.CellButtonA4.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonA4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonA4.FlatAppearance.BorderSize = 0;
            this.CellButtonA4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonA4.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonA4.Location = new System.Drawing.Point(600, 0);
            this.CellButtonA4.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonA4.Name = "CellButtonA4";
            this.CellButtonA4.Size = new System.Drawing.Size(200, 150);
            this.CellButtonA4.TabIndex = 4;
            this.CellButtonA4.Text = "A4";
            this.CellButtonA4.UseVisualStyleBackColor = false;
            this.CellButtonA4.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonB1
            // 
            this.CellButtonB1.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonB1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonB1.FlatAppearance.BorderSize = 0;
            this.CellButtonB1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonB1.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonB1.Location = new System.Drawing.Point(0, 150);
            this.CellButtonB1.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonB1.Name = "CellButtonB1";
            this.CellButtonB1.Size = new System.Drawing.Size(200, 150);
            this.CellButtonB1.TabIndex = 5;
            this.CellButtonB1.Text = "B1";
            this.CellButtonB1.UseVisualStyleBackColor = false;
            this.CellButtonB1.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonB2
            // 
            this.CellButtonB2.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonB2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonB2.FlatAppearance.BorderSize = 0;
            this.CellButtonB2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonB2.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonB2.Location = new System.Drawing.Point(200, 150);
            this.CellButtonB2.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonB2.Name = "CellButtonB2";
            this.CellButtonB2.Size = new System.Drawing.Size(200, 150);
            this.CellButtonB2.TabIndex = 6;
            this.CellButtonB2.Text = "B2";
            this.CellButtonB2.UseVisualStyleBackColor = false;
            this.CellButtonB2.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonB3
            // 
            this.CellButtonB3.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonB3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonB3.FlatAppearance.BorderSize = 0;
            this.CellButtonB3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonB3.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonB3.Location = new System.Drawing.Point(400, 150);
            this.CellButtonB3.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonB3.Name = "CellButtonB3";
            this.CellButtonB3.Size = new System.Drawing.Size(200, 150);
            this.CellButtonB3.TabIndex = 7;
            this.CellButtonB3.Text = "B3";
            this.CellButtonB3.UseVisualStyleBackColor = false;
            this.CellButtonB3.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonB4
            // 
            this.CellButtonB4.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonB4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonB4.FlatAppearance.BorderSize = 0;
            this.CellButtonB4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonB4.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonB4.Location = new System.Drawing.Point(600, 150);
            this.CellButtonB4.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonB4.Name = "CellButtonB4";
            this.CellButtonB4.Size = new System.Drawing.Size(200, 150);
            this.CellButtonB4.TabIndex = 8;
            this.CellButtonB4.Text = "B4";
            this.CellButtonB4.UseVisualStyleBackColor = false;
            this.CellButtonB4.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonC1
            // 
            this.CellButtonC1.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonC1.FlatAppearance.BorderSize = 0;
            this.CellButtonC1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonC1.Location = new System.Drawing.Point(0, 300);
            this.CellButtonC1.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonC1.Name = "CellButtonC1";
            this.CellButtonC1.Size = new System.Drawing.Size(200, 150);
            this.CellButtonC1.TabIndex = 9;
            this.CellButtonC1.Text = "C1";
            this.CellButtonC1.UseVisualStyleBackColor = false;
            this.CellButtonC1.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonC2
            // 
            this.CellButtonC2.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonC2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonC2.FlatAppearance.BorderSize = 0;
            this.CellButtonC2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonC2.Location = new System.Drawing.Point(200, 300);
            this.CellButtonC2.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonC2.Name = "CellButtonC2";
            this.CellButtonC2.Size = new System.Drawing.Size(200, 150);
            this.CellButtonC2.TabIndex = 10;
            this.CellButtonC2.Text = "C2";
            this.CellButtonC2.UseVisualStyleBackColor = false;
            this.CellButtonC2.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonC3
            // 
            this.CellButtonC3.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonC3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonC3.FlatAppearance.BorderSize = 0;
            this.CellButtonC3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonC3.Location = new System.Drawing.Point(400, 300);
            this.CellButtonC3.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonC3.Name = "CellButtonC3";
            this.CellButtonC3.Size = new System.Drawing.Size(200, 150);
            this.CellButtonC3.TabIndex = 11;
            this.CellButtonC3.Text = "C3";
            this.CellButtonC3.UseVisualStyleBackColor = false;
            this.CellButtonC3.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonC4
            // 
            this.CellButtonC4.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonC4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonC4.FlatAppearance.BorderSize = 0;
            this.CellButtonC4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonC4.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonC4.Location = new System.Drawing.Point(600, 300);
            this.CellButtonC4.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonC4.Name = "CellButtonC4";
            this.CellButtonC4.Size = new System.Drawing.Size(200, 150);
            this.CellButtonC4.TabIndex = 12;
            this.CellButtonC4.Text = "C4";
            this.CellButtonC4.UseVisualStyleBackColor = false;
            this.CellButtonC4.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonD1
            // 
            this.CellButtonD1.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonD1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonD1.FlatAppearance.BorderSize = 0;
            this.CellButtonD1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonD1.Location = new System.Drawing.Point(0, 450);
            this.CellButtonD1.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonD1.Name = "CellButtonD1";
            this.CellButtonD1.Size = new System.Drawing.Size(200, 150);
            this.CellButtonD1.TabIndex = 13;
            this.CellButtonD1.Text = "D1";
            this.CellButtonD1.UseVisualStyleBackColor = false;
            this.CellButtonD1.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonD2
            // 
            this.CellButtonD2.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonD2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonD2.FlatAppearance.BorderSize = 0;
            this.CellButtonD2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonD2.Location = new System.Drawing.Point(200, 450);
            this.CellButtonD2.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonD2.Name = "CellButtonD2";
            this.CellButtonD2.Size = new System.Drawing.Size(200, 150);
            this.CellButtonD2.TabIndex = 14;
            this.CellButtonD2.Text = "D2";
            this.CellButtonD2.UseVisualStyleBackColor = false;
            this.CellButtonD2.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonD3
            // 
            this.CellButtonD3.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonD3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonD3.FlatAppearance.BorderSize = 0;
            this.CellButtonD3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonD3.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonD3.Location = new System.Drawing.Point(400, 450);
            this.CellButtonD3.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonD3.Name = "CellButtonD3";
            this.CellButtonD3.Size = new System.Drawing.Size(200, 150);
            this.CellButtonD3.TabIndex = 15;
            this.CellButtonD3.Text = "D3";
            this.CellButtonD3.UseVisualStyleBackColor = false;
            this.CellButtonD3.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonD4
            // 
            this.CellButtonD4.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonD4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonD4.FlatAppearance.BorderSize = 0;
            this.CellButtonD4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonD4.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonD4.Location = new System.Drawing.Point(600, 450);
            this.CellButtonD4.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonD4.Name = "CellButtonD4";
            this.CellButtonD4.Size = new System.Drawing.Size(200, 150);
            this.CellButtonD4.TabIndex = 16;
            this.CellButtonD4.Text = "D4";
            this.CellButtonD4.UseVisualStyleBackColor = false;
            this.CellButtonD4.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // CellButtonA2
            // 
            this.CellButtonA2.BackColor = System.Drawing.SystemColors.Control;
            this.CellButtonA2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CellButtonA2.FlatAppearance.BorderSize = 0;
            this.CellButtonA2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CellButtonA2.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CellButtonA2.Location = new System.Drawing.Point(200, 0);
            this.CellButtonA2.Margin = new System.Windows.Forms.Padding(0);
            this.CellButtonA2.Name = "CellButtonA2";
            this.CellButtonA2.Size = new System.Drawing.Size(200, 150);
            this.CellButtonA2.TabIndex = 17;
            this.CellButtonA2.Text = "A2";
            this.CellButtonA2.UseVisualStyleBackColor = false;
            this.CellButtonA2.Click += new System.EventHandler(this.CellButton_Click);
            // 
            // PictureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.tlp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "PictureForm";
            this.Text = "PictureForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PictureForm_FormClosing);
            this.tlp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button CellButtonA1;
        private System.Windows.Forms.Button CellButtonA3;
        private System.Windows.Forms.Button CellButtonA4;
        private System.Windows.Forms.Button CellButtonB1;
        private System.Windows.Forms.Button CellButtonB2;
        private System.Windows.Forms.Button CellButtonB3;
        private System.Windows.Forms.Button CellButtonB4;
        private System.Windows.Forms.Button CellButtonC1;
        private System.Windows.Forms.Button CellButtonC2;
        private System.Windows.Forms.Button CellButtonC3;
        private System.Windows.Forms.Button CellButtonC4;
        private System.Windows.Forms.Button CellButtonD1;
        private System.Windows.Forms.Button CellButtonD2;
        private System.Windows.Forms.Button CellButtonD3;
        private System.Windows.Forms.Button CellButtonD4;
        private System.Windows.Forms.Button CellButtonA2;
        internal System.Windows.Forms.TableLayoutPanel tlp;
    }
}