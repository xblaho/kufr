﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Kufr
{
    public partial class MainForm : Form
    {
        private string m_imageFolder;
        internal Dictionary<string, string> Descriptions { get; } = new Dictionary<string, string>();
        internal string PlayerName { get; private set; } = "Soutěžící";

        public MainForm()
        {
            InitializeComponent();
            folderBrowserDialog.SelectedPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            folderBrowserDialog.ShowDialog();
            m_imageFolder = folderBrowserDialog.SelectedPath + "\\";
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.Show();
            this.WindowState = FormWindowState.Normal;
            RefreshImageList();
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            if (listBox.SelectedIndex < 0)
                return;
            var fileName = m_imageFolder + (string)listBox.SelectedItem;
            var pf = new PictureForm(this);
            using (var img = Image.FromFile(fileName))
            {
                //pf.Height = (pf.tlp.Width * img.Height / img.Width) + (pf.Height - pf.tlp.Height);
                pf.BackgroundImage = img;
                pf.Text = Path.GetFileNameWithoutExtension(fileName);
                pf.ShowDialog();
            }
            File.Move(fileName, fileName.Substring(0, fileName.Length - 1));
            RefreshImageList();
        }

        private void RestoreButton_Click(object sender, EventArgs e)
        {
            Directory.GetFiles(m_imageFolder, "*.jp").ToList().ForEach(f => File.Move(f, f + "g"));
            RefreshImageList();
        }

        private void RefreshImageList()
        {
            listBox.Items.Clear();
            Directory.GetFiles(m_imageFolder, "*.jpg").Select(f => Path.GetFileName(f)).ToList().ForEach(f => listBox.Items.Add(f));
            LoadDescriptions();
        }

        private void LoadDescriptions()
        {
            Descriptions.Clear();
            if (!File.Exists(m_imageFolder + "descript.ion"))
                return;
            using (var fs = File.OpenText(m_imageFolder + "descript.ion"))
            {
                while (!fs.EndOfStream)
                {
                    var line = fs.ReadLine();
                    string key, val = String.Empty;
                    if (line.StartsWith("\""))
                    {
                        key = Path.GetFileNameWithoutExtension(line.Substring(1, line.IndexOf('"', 1) - 1));
                        val = line.Substring(line.IndexOf('"', 1) + 2, line.Length - line.IndexOf('"', 1) - 2);
                    }
                    else
                    {
                        key = Path.GetFileNameWithoutExtension(line.Substring(0, line.IndexOf(' ', 1)));
                        val = line.Substring(line.IndexOf(' ', 1) + 1, line.Length - line.IndexOf(' ', 1) - 1);
                    }

                    Descriptions.Add(key, Regex.Unescape(val.Replace("\x04\xfffd", "")));
                }
            }
        }
    }
}
