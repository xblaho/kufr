﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Kufr
{
    public partial class PictureForm : Form
    {
        private MainForm m_mainForm;

        public PictureForm()
        {
            InitializeComponent();
        }

        public PictureForm(MainForm mainForm) : this()
        {
            m_mainForm = mainForm;
        }

        private void CellButton_Click(object sender, EventArgs e)
        {
            tlp.SuspendLayout();
            ((Control)sender).Visible = false;
            tlp.ResumeLayout();
        }

        private void PictureForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var body = tlp.Controls.OfType<Button>().Count(b => b.Visible);
            tlp.Visible = false;
            new NextPictureForm(m_mainForm.Descriptions.ContainsKey(this.Text) ? m_mainForm.Descriptions[this.Text] + "\n\n" : String.Empty, $"{m_mainForm.PlayerName} získal: {body} bodů").ShowDialog();
        }
    }
}
